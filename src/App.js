import logo from "./logo.svg";
import "./App.css";
import NavScrollExample from "./components/navbar";

import "bootstrap/dist/css/bootstrap.min.css";
import Boards from "./components/boards";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
// import BasicExample from "./components/cardsDetails";
import CardList from "./components/AddLists";

function App() {
  return (
    <>
    <NavScrollExample />

    <div className="App">
      <div>
      </div>
      <Router>
        <Switch>
          <Route exact path = "/">
            <div
              style={{
                padding: "10px",
                display: "flex",
                flexWrap: "wrap",
                justifyContent: "center",
              }}
            >
              <Boards />
            </div>
          </Route>
          <Route path="/:id" component={CardList}>
              
          
          </Route>
        </Switch>
      </Router>
    </div>
    </>
    );
}

export default App;
