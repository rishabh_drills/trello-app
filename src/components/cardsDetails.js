import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

function BasicExample(props) {

  return (
    <>
    <Link
      to={`/${props.board.id}`}
      
      style={{
        margin: "2rem",
        border: "1px solid black",
        width: "15rem",
        height: "15rem",
        boxShadow: "5px 5px 5px 5px #9E9E9E"
        
      }}
    >
    <img width='100%' height='80%' src="https://placeimg.com/200/100/nature" alt=""/>
      {props.board.name}
    </Link>
    
    </>
  );
}

export default BasicExample;
