import axios from "axios";
import React, { Component } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import CheckItems from "./CheckItems";

class CheckList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      checklistAdd: [],
      checklistName: "",
    };
    this.showModal = this.showModal.bind(this);
    this.hideModal = this.hideModal.bind(this);
  }

  showModal = () => {
    this.setState({ show: true });
  };

  hideModal = () => {
    this.setState({ show: false });
  };

  componentDidMount() {
    axios
      .get(
        `https://api.trello.com/1/cards/${this.props.checklistId}/checklists?key=9a900b1d5f9be1d558c23494cdf4697c&token=4ebdcd4d7720abbf4f822bfbaba0c13f400ce7b528bbe39926dad53e965999e1`
      )
      .then((res) => {
        this.setState({
          checklistAdd: res.data,
        });
      });
  }

  createChecklist = (e, id) => {
    e.preventDefault();
    axios
      .post(
        `https://api.trello.com/1/checklists?idCard=${this.props.checklistId}&name=${this.state.checklistName}&key=9a900b1d5f9be1d558c23494cdf4697c&token=4ebdcd4d7720abbf4f822bfbaba0c13f400ce7b528bbe39926dad53e965999e1`
      )
      .then((res) => {
        console.log("checklistAdd");
        this.setState({
          checklistAdd: [...this.state.checklistAdd, res.data],
          checklistName: "",
        });
      });
  };

  deleteChecklist = async (checklist) => {
    await axios
      .delete(
        `https://api.trello.com/1/checklists/${checklist}?key=9a900b1d5f9be1d558c23494cdf4697c&token=4ebdcd4d7720abbf4f822bfbaba0c13f400ce7b528bbe39926dad53e965999e1`
      )
      .then((res) => {
        console.log(res);

        this.setState({
          checklistAdd: this.state.checklistAdd.filter((newChecklist) => {
            if (newChecklist.id !== checklist) {
              return newChecklist;
            }
          }),
        });
      });
  };

  render() {
    return (
      <>
        <Button
          style={{ backgroundColor: "green", margin: "5px" }}
          onClick={this.showModal}
        >
          Checklist
        </Button>

        <Modal show={this.state.show} onHide={this.hideModal}>
          <Modal.Header closeButton>
            <Modal.Title>Create Checklist</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <form style={{ margin: "5px" }}>
              <input
                type="text"
                value={this.state.checklistName}
                onChange={(e) => {
                  this.setState({ checklistName: e.target.value });
                }}
              ></input>
              <button onClick={(e) => this.createChecklist(e)}>Add</button>
            </form>
            {this.state.checklistAdd.map((check) => {
              return (
                <div
                  key={check.id}
                  style={{
                    border: "1px solid black",
                    color: "blue",
                    fontWeight: "bold",
                    margin: "5px",
                  }}
                >
                  {check.name}
                  <CheckItems check={check} cardId ={this.props.checklistId} />
                  <div>
                    <button
                      style={{
                        backgroundColor: "red",
                        color: "white",
                        margin: "2px",
                      }}
                      onClick={() => this.deleteChecklist(check.id)}
                    >
                      Delete
                    </button>
                  </div>
                </div>
              );
            })}
          </Modal.Body>
          <Modal.Footer>
            <Button variant="primary" onClick={this.hideModal}>
              Save Changes
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
  }
}

export default CheckList;
