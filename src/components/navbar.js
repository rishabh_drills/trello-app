import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Form from "react-bootstrap/Form";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import NavDropdown from "react-bootstrap/NavDropdown";

function NavScrollExample() {
  return (
    <Navbar style={{ backgroundColor: "#026AA7" }} expand="lg">
      <Container fluid>
        <img
          style={{ width: "7%", padding: "1rem" }}
          src="https://seeklogo.com/images/T/trello-logo-45ABCC6452-seeklogo.com.png"
          alt=""
        />

        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
          <Nav
            className="me-auto my-2 my-lg-0"
            style={{ maxHeight: "100px" }}
            navbarScroll
          >
            <Nav.Link href="#action1" style={{ color: "white" }}>
              Workspace
            </Nav.Link>
            <Nav.Link href="#action2" style={{ color: "white" }}>
              Recent
            </Nav.Link>
            <Nav.Link href="#" style={{ color: "white" }}>
              Templates
            </Nav.Link>
          </Nav>
          <Form className="d-flex">
            <Form.Control
              type="search"
              placeholder="Search"
              className="me-2"
              aria-label="Search"
            />
            <Button variant="outline-success" style={{ color: "white" }}>
              Search
            </Button>
          </Form>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default NavScrollExample;
