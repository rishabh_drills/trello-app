import React from "react";
import axios from "axios";
// import CardsDetails from "./cardsDetails"

import { Link } from "react-router-dom";
import CardsOnList from "./CardsOnList";

class CardList extends React.Component {
  state = {
    listDetails: [],
  };

  componentDidMount(){
    axios.get(`https://api.trello.com/1/boards/${this.props.match.params.id}/lists?key=9a900b1d5f9be1d558c23494cdf4697c&token=4ebdcd4d7720abbf4f822bfbaba0c13f400ce7b528bbe39926dad53e965999e1`)
    .then((res) => this.setState({
        listDetails: res.data
    }))
  }

  createList = async(e) => {console.log(e.target[0].value)
    e.preventDefault();
    
    await axios.post(`https://api.trello.com/1/boards/${this.props.match.params.id}/lists?name=${e.target[0].value}&key=9a900b1d5f9be1d558c23494cdf4697c&token=4ebdcd4d7720abbf4f822bfbaba0c13f400ce7b528bbe39926dad53e965999e1`)
    .then((res) =>{
      this.setState({
        listDetails: [...this.state.listDetails, res.data],
      })
    })
    .catch((error)=>{
      console.error(error)
    })
  }

  archiveList = (id) => {
    axios
      .put(
        `https://api.trello.com/1/lists/${id}/closed?key=9a900b1d5f9be1d558c23494cdf4697c&token=4ebdcd4d7720abbf4f822bfbaba0c13f400ce7b528bbe39926dad53e965999e1&value=true`
      )
      .then((res) => console.log("Archived"));

      this.setState({
        listDetails: this.state.listDetails.filter((item) => {
          if(item.id !== id){
            return item
          }
        })
      })    
  };

  render() {
    console.log(this.props)
    return (
      <div style={{ display: "flex", flexWrap: "wrap" }}>
        {this.state.listDetails.length === 0
          ? ""
          : this.state.listDetails.map((a) => {
              return <CardsOnList  key={a.id} board={a} listId={a.id} archiveList={this.archiveList}/>;
            })}
        <div
          
          style={{
            margin: "2rem",
            border: "1px solid black",
            width: "15rem",
            height: "7rem",
            
            backgroundColor: "#F4F4F4",
            boxShadow: "5px 5px 5px 5px #9E9E9E",
          }}
        >
          <div style={{padding:'2%'}} >
            <h5>Create a New List</h5>
            <form  onSubmit={(e) => {this.createList(e)}}>
              <input
                style={{ width: "70%"}}
                type="text"
              ></input>
              <button type="submit" style={{margin:'10px'}}
              >
                Add List
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default CardList;


