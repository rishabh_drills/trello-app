import axios from "axios";
import React, { Component } from "react";
import { Form } from "react-bootstrap";

class CheckItems extends Component {
  state = {
    items: [],
    itemsName: "",
  };

  componentDidMount() {
    axios
      .get(
        `https://api.trello.com/1/checklists/${this.props.check.id}/checkItems?key=9a900b1d5f9be1d558c23494cdf4697c&token=4ebdcd4d7720abbf4f822bfbaba0c13f400ce7b528bbe39926dad53e965999e1`
      )
      .then((res) => {
        console.log(res.data);
        this.setState({
          items: res.data,
        });
      });
  }

  addItems = (e) => {
    e.preventDefault();
    axios
      .post(
        `https://api.trello.com/1/checklists/${this.props.check.id}/checkItems?name=${this.state.itemsName}&key=9a900b1d5f9be1d558c23494cdf4697c&token=4ebdcd4d7720abbf4f822bfbaba0c13f400ce7b528bbe39926dad53e965999e1`
      )
      .then((res) => {
        console.log(res.data);
        this.setState({
          items: [...this.state.items, res.data],
          itemsName: "",
        });
      });
  };

  deleteItems = async (itemsId) => {
    // e.preventDefault();
    await axios
      .delete(
        `https://api.trello.com/1/checklists/${this.props.check.id}/checkItems/${itemsId}?key=9a900b1d5f9be1d558c23494cdf4697c&token=4ebdcd4d7720abbf4f822bfbaba0c13f400ce7b528bbe39926dad53e965999e1`
      )
      .then((res) => {
        console.log(res);

        this.setState({
          items: this.state.items.filter((newItems) => {
            if (newItems.id !== itemsId) {
              return newItems;
            }
          }),
        });
      });
  };

  CheckItemTickBox = (status, idCheckItem, id) => {
    let state;
    if (status === true) {
      state = "complete";
    } else {
      state = "incomplete";
    }
    axios
      .put(
        `https://api.trello.com/1/cards/${id}/checkItem/${idCheckItem}?key=9a900b1d5f9be1d558c23494cdf4697c&token=4ebdcd4d7720abbf4f822bfbaba0c13f400ce7b528bbe39926dad53e965999e1`,
        null,
        {
          params: { state },
        }
      )
      .then((res) => {
        let items = this.state.items.filter((update) => {
            // console.log(res);
          if (update.id === res.data.id) {
            update.state = state;
           
            return update;
          } else {
            return update;
          }
        });
        this.setState({
          items,
        });
      });
  };

  render() {
    let status;
    return (
      <div>
        {this.state.items.map((item) => {
          if (item.state === "complete") {
            status = true;
          } else {
            status = false;
          }
          console.log(item);
          return (
            <div
              key={item.id}
              style={{
                border: "1px solid black",
                color: "black",
                fontWeight: "normal",
                width: "20vw",
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <Form>
                <Form.Group>
                  <Form.Check
                    type="checkbox"
                    checked={status}
                    label={item.name}
                    onChange={(e) => {
                      this.CheckItemTickBox(
                        e.target.checked,
                        item.id,
                        this.props.cardId
                      );
                    }}
                  ></Form.Check>
                  
                </Form.Group>
              </Form>
              <button
                  type="button"
                    style={{ backgroundColor: "red" }}
                    onClick={() => this.deleteItems(item.id)}
                  >
                    Delete
                  </button>
             
            </div>
          );
        })}
        <form>
          <button
            style={{
              backgroundColor: "blue",
              color: "white",
              margin: " 2px ",
            }}
            onClick={(e) => {
              this.addItems(e);
            }}
          >
            Add an item
          </button>
          <input
            type="text"
            value={this.state.itemsName}
            onChange={(e) => {
              this.setState({ itemsName: e.target.value });
            }}
          />
        </form>
      </div>
    );
  }
}

export default CheckItems;
