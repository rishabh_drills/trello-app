// import Button from "react-bootstrap/Button";
// import Card from "react-bootstrap/Card";
import { Component } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import axios from "axios";

import React from "react";
import CheckList from "./CheckList";

class CardsOnList extends Component {
  state = {
    cards: [],
    
  };

  componentDidMount() {
    axios
      .get(
        `https://api.trello.com/1/lists/${this.props.listId}/cards?key=9a900b1d5f9be1d558c23494cdf4697c&token=4ebdcd4d7720abbf4f822bfbaba0c13f400ce7b528bbe39926dad53e965999e1`
      )
      .then((res) =>
        this.setState({
          cards: res.data,
        })
      );
  }

  addCard = () => {
    const cardName = prompt();
    if(cardName.length === 0 || cardName.length === null){
      alert('Please Enter Text')
    }else{
    axios
      .post(
        `https://api.trello.com/1/lists/${this.props.listId}/cards?key=9a900b1d5f9be1d558c23494cdf4697c&token=4ebdcd4d7720abbf4f822bfbaba0c13f400ce7b528bbe39926dad53e965999e1&name=${cardName}`
      )
      .then((res) => {
        this.setState({
          cards: [...this.state.cards, res.data],
        });
      });
    }
    console.log(this.state.cards)
  };

  removeCard = async (cardId) => {
    await axios.delete(
      `https://api.trello.com/1/cards/${cardId}?key=9a900b1d5f9be1d558c23494cdf4697c&token=4ebdcd4d7720abbf4f822bfbaba0c13f400ce7b528bbe39926dad53e965999e1`
    );

    axios
      .get(
        `https://api.trello.com/1/lists/${this.props.listId}/cards?key=9a900b1d5f9be1d558c23494cdf4697c&token=4ebdcd4d7720abbf4f822bfbaba0c13f400ce7b528bbe39926dad53e965999e1`
      )
      .then((res) =>
        this.setState({
          cards: res.data,
        })
        
      );
      
  };

  render() {
    return (
      <>
        <div
          style={{
            margin: "2rem",
            border: "1px solid black",
            width: "15rem",
            boxShadow: "5px 5px 5px 5px #9E9E9E",
            display: "flex",
            flexDirection: "column",
          }}
        >
          {this.props.board.name}

          <div style={{ width: "100%" }}>
            {this.state.cards.map((card) => {
              return (
                <div
                  style={{
                    border: "1px solid blue",
                    margin: "10px",
                    boxShadow: "2px 2px 2px 2px #9E9E9E",
                  }}
                  key={card.id}
                >
                  <img
                    width="100%"
                    height="80%"
                    src="https://placeimg.com/200/100/nature"
                    alt=""
                  />
                  <p>{card.name}</p>
                  <button
                    style={{ backgroundColor: "red", color: "white" }}
                    onClick={() => this.removeCard(card.id)}
                  >
                    Remove Card
                  </button>
                  <div>
                    <CheckList checklistId={card.id} 
                    />
                  </div>
                </div>
                
              );
            })}
            <button
              style={{
                margin: "10px",
                backgroundColor: "blue",
                color: "white",
              }}
              onClick={this.addCard}
            >
              Add Card
            </button>

            <span style={{ margin: "10px" }}>
              <button
                onClick={()=>this.props.archiveList(this.props.listId)}
                style={{ backgroundColor: "red", color: "white" }}
              >
                ArchiveList
              </button>
            </span>
          </div>
        </div>
      </>
    );
  }
}

export default CardsOnList;
