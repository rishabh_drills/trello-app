import React from "react";
import axios from "axios";
import CardsDetails from "./cardsDetails";



class Boards extends React.Component {
  state = {
    listName: [],
    boardName: ""
  };

  componentDidMount() {
    axios
      .get(
        "https://api.trello.com/1/members/me/boards?key=9a900b1d5f9be1d558c23494cdf4697c&token=4ebdcd4d7720abbf4f822bfbaba0c13f400ce7b528bbe39926dad53e965999e1"
      )
      .then((res) => {
        this.setState({
          listName: res.data,
        });
    
      });
  }

  createBoards = (e) => {
    e.preventDefault();
    axios
      .post(
        `https://api.trello.com/1/boards/?name=${this.state.boardName}&key=9a900b1d5f9be1d558c23494cdf4697c&token=4ebdcd4d7720abbf4f822bfbaba0c13f400ce7b528bbe39926dad53e965999e1`
      )
      
      .catch((error) => {
        console.error(error);
      });
  };

  componentDidUpdate(prevProps, prevState){
    if(prevState.listName !== this.state.listName){
    axios
      .get(
        "https://api.trello.com/1/members/me/boards?key=9a900b1d5f9be1d558c23494cdf4697c&token=4ebdcd4d7720abbf4f822bfbaba0c13f400ce7b528bbe39926dad53e965999e1"
      )
      .then((res) => {
        this.setState({
          listName: res.data,
        });
      });
    }
  }

  render() {
    return (
      <div style={{ display: "flex", flexWrap: "wrap" }}>
        {this.state.listName.length === 0 ? (
         ""
        ) : (
          this.state.listName.map((a) => {
            return <CardsDetails key={a.id} board={a} />;
          })
          
        )}
        <div
          
          style={{
            margin: "1rem",
            border: "1px solid black",
            width: "15rem",
            height: "15rem",
            marginTop:'2rem',
            backgroundColor:'#F4F4F4',
            boxShadow: "5px 5px 5px 5px #9E9E9E"
            
          }}
        >
          <div style={{padding:'10%'}}>
            <p>Create a new board</p>
            <form>
              <input value={this.state.boardName} onChange={(e)=>{this.setState({boardName:e.target.value})}} style={{ width: "100%" }} type="text"></input>
              <button style={{margin:'20px'}} onClick={(e) => {
                this.createBoards(e);
              }}>Create</button>
              </form>
          </div>
        </div>

        

      </div>
    );
  }
}

export default Boards;
